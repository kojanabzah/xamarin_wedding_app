﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parse;

namespace Shared.Helpers
{
    public delegate void ChangedEventHandler(object sender, EventArgs e);

    public class AdminHelper
    {
        private static AdminHelper _instance;
        private UserHelper userHelper;
        private ServerHelperUtil _serverHelperUtil;

        private List<Event> _waitingConfirmation;

        public ChangedEventHandler ConfirmationListChanged;

        private  AdminHelper()
        {
            userHelper = new UserHelper();
            _serverHelperUtil = ServerHelperUtil.GetInstance();

        }

        public static AdminHelper GetInstance()
        {
            if (_instance == null)
                _instance = new AdminHelper();

            return _instance;
        }

        private void OnConfirmationListChange(EventArgs e) 
        {
            if (ConfirmationListChanged != null)
                ConfirmationListChanged(this, e);
            
        }

        private List<Event> InitCinfirmationList()
        {
            var list = new List<Event>();
            if (!userHelper.IsAdmin()) return list; //If not admin dont get the list from server 

            var items = _serverHelperUtil.GetWeddings();

            list = items.Where(t => !t.IsConfirmd).ToList();

            return list;
        } 

        public List<Event> GetWaitingConfirmationList()
        {
            _waitingConfirmation = InitCinfirmationList();

            return _waitingConfirmation;
        }


        public async void ConfirmEvent(string objectId)
        {
            var query = from eventItem in ParseObject.GetQuery("WeddingEvents")
                        where eventItem.Get<string>("objectId") ==  objectId
                        select eventItem;

            var result = await query.FindAsync();

            var thisEvent = result.FirstOrDefault();
            if (thisEvent == null) return;

            thisEvent["IsConfirmed"] = true;

            await thisEvent.SaveAsync();

            var update = _serverHelperUtil._comingWeddings.FirstOrDefault(t => t.ObjectId == objectId);
            update.IsConfirmd = true;


            OnConfirmationListChange(EventArgs.Empty);
        }



    }
}
