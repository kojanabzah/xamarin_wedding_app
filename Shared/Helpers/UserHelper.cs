﻿using System.Runtime.CompilerServices;
using Parse;
namespace Shared.Helpers
{
    public class UserHelper
    {
        private ParseUser _user;

        private const string Administrator = "Administrator";

        public enum UserType
        {
            Administrator,
            Regular

        }

        public bool IsAdmin()
        {
            if (ParseUser.CurrentUser == null) return false;

            string userType;
            if (ParseUser.CurrentUser.TryGetValue("UserType", out userType))
            {
                if (userType == Administrator) return true;
            }

            return false;
        }

        public bool IsConnected()
        {
            if (ParseUser.CurrentUser == null) return false;

            return true;
        }


    }
}
