﻿using System;
using Parse;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Shared
{
	public class ServerHelperUtil 
	{

		private static ServerHelperUtil _instance;
		private WeddingEventsHelper _weddingEventHelper;

		private List<GalleryItem> _imageList;

		public List<Event> _comingWeddings;
		private List<Event> _weddings;

		public bool IsRefreshNeeded{ get; set;}




		private ServerHelperUtil(){
			_imageList = new List<GalleryItem> ();
			_weddingEventHelper = new WeddingEventsHelper ();
		}

		public static ServerHelperUtil GetInstance(){
			if (_instance == null)
				_instance = new ServerHelperUtil ();

			return _instance;
		}


		public async void GetEventImages(string eventId){
			//TODO use eventID

			_imageList.Clear();

			ParseQuery<ParseObject> query = ParseObject.GetQuery("WeddingImages");


			await query.FindAsync ().ContinueWith (t => {

				try{
					IList<ParseObject> results = (IList<ParseObject>)t.Result;
					long id = 1;
					foreach (ParseObject obj in results){

						var item = new GalleryItem(obj.Get<ParseFile>("image"),id);
						_imageList.Add(item);
						id++;

					}
						

				}
				catch(Exception ex){
					if(ex.Message !=null){
					}

				}
			},TaskScheduler.FromCurrentSynchronizationContext());

		}

		public GalleryItem GetImage(long id){

			if (_imageList == null || _imageList.Count == 0)
				return null;

			return  _imageList.First (x => x.Id == id);
		}


		public List<GalleryItem> GetEventImageList(){
			return _imageList;
		}


		private async Task InitWeddingList()
		{
		    ParseQuery<ParseObject> query = ParseObject.GetQuery("WeddingEvents");

			var result = await query.FindAsync();

			_weddings = new List<Event> ();
			_comingWeddings = new List<Event> ();

			var newMonth = DateTime.Today;

			foreach (var item in result) {

				var groom = item.Get<string>("GroomName");
				var bride = item.Get<string>("BrideName");
				var type  = item.Get<string>("EventType");
                var isConfirmed = item.Get<bool>("IsConfirmed");

				var id = item.ObjectId;

				var displayText = string.Format ("{0} & {1}", groom, bride);
				var date = item.Get<DateTime> ("Date");

				if (newMonth.Month < date.Month)
				{
				    var dummyEvent = new Event
				    {
				        Date = date,
				        GroomName = "",
				        BrideName = "",
				        DisplayText = "",
				        type = "",
				        IsConfirmd = true
				    };
                    _weddings.Add(dummyEvent);
                   // _comingWeddings.Add(dummyEvent);
				}

			    var newEvent = new Event
			    {
			        Date = date,
			        GroomName = groom,
			        BrideName = bride,
			        DisplayText = displayText,
			        type = type,
			        ObjectId = id,
                    IsConfirmd = isConfirmed
			    };
               // _comingWeddings.Add(newEvent); //TODO add only next comming dates
                _weddings.Add(newEvent);


                _comingWeddings = _weddings.Where(t => t.Date > DateTime.Now).ToList();
				newMonth = date;
			}

		}

		public async Task<List<Event>> GetComingWeddingEvents(){

			await InitWeddingList ();

			return _comingWeddings.Where(t=>t.Date > DateTime.Now).ToList();
		}

		public List<Event> GetWeddings(){
			return _weddings;
		}


			

	}
}

