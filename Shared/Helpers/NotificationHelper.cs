﻿using System;
using System.Collections.Generic;
using System.Text;
using Android.Content;
using Java.Lang;
using Parse;

namespace Shared.Helpers
{
    public class NotificationHelper
    {
        public async void SendNotificationToAdmins(string message)
        {
            try
            {
                var push = new ParsePush();
                push.Channels = new List<string> {"Admins"};
                push.Data = new Dictionary<string, object>
                {
                    {"alert", message},
                    {"admins",""},
                };
                await push.SendAsync();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public async void SendNotificationToAll(string message)
        {
            try
            {
                var push = new ParsePush();
                push.Data = new Dictionary<string, object>
                {
                    {"alert", message},
                };
                await push.SendAsync();
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
