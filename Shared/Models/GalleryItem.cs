﻿using System;

using Parse;

namespace Shared
{
	public class GalleryItem
	{
		public ParseFile file;
		public long Id;

		public GalleryItem (ParseFile file,long id)
		{
			this.file = file;
			this.Id = id;
		}

		public byte[] byteFile{ get; set;}

	}
}

