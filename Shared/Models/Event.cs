﻿using System;

namespace Shared
{
	public class Event
	{
		public long Id { get; set; }
		public string ObjectId { get; set; }
		public string DisplayText{ get; set; }
		public string GroomName{ get; set; }
		public string BrideName{ get; set; }
		public DateTime Date { get; set; }
		public string type { get; set; }
	    public bool IsConfirmd { get; set; }

	}
}

