﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Java.Lang;
using Parse;
using WeddingAndroid.Activities;
using Exception = System.Exception;
using String = System.String;


namespace WeddingAndroid
{
    [Application]
	public class MainApp: Application
    {
        private const string MainTag = "MainApp";
		public MainApp (IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
		{
		}

		public override void OnCreate ()
		{
			base.OnCreate ();


		    try
		    {
		        // Initialize the parse client with your Application ID and .NET Key found on
		        // your Parse dashboard
		        ParseClient.Initialize("a2xGfyyUhE13njO5wseWx4NFiL8OjtwKdzJizRtk",
		            "xw5uYIugAgCrk1nROffBWNk2ureQRv4r7jYdUJXJ");


		        ParseFacebookUtils.Initialize("479500575549789");
		        // ParsePush.ParsePushNotificationReceived += ParsePush.DefaultParsePushNotificationReceivedHandler;



		        ParsePush.ParsePushNotificationReceived += (sender, args) =>
		        {
		            var payload = args.Payload;
		            object objectId;

                 
		            object msg;
                    
                    if (payload.TryGetValue("alert", out msg))
                    {
                        object temp;
                        if (payload.ContainsKey("admins"))
                        {
                            DisplayAlertToAdmin(msg as string);
                        }
                        else
                            DisplayAlert(msg as string);
                    }
		            else if (payload.TryGetValue("objectId", out objectId))
		            {
                        DisplayNotificationWithObjectId(objectId as string);
		            }
		        };
		    }
		    catch (Exception ex)
		    {
		        Log.Error(MainTag, ex.Message);
		    }
		}


        private void DisplayAlertToAdmin(string msg)
        {
            var intent = new Intent(this, typeof(AdminActivity));
            var contentIntent = PendingIntent.GetActivity(this, 0, intent, 0);

            Notification.Builder builder = new Notification.Builder(this)
                  .SetContentTitle("למנהל חתונות")
                  .SetContentText("חתונה חדשה מחכה לאישור")
                  .SetContentIntent(contentIntent)
                  .SetSmallIcon(Resource.Drawable.WedType);

            // Build the notification:
            Notification notification = builder.Build();

            // Get the notification manager:
            var notificationManager =
                GetSystemService(Context.NotificationService) as NotificationManager;

            // Publish the notification:
            const int notificationId = 0;
            notificationManager.Notify(notificationId, notification);
        }
        private void DisplayAlert(string alert)
        {
            try
            {
               
                var intent = new Intent(this, typeof(WeddingCalendar)); 
                var contentIntent = PendingIntent.GetActivity(this, 0, intent, 0);

                Notification.Builder builder = new Notification.Builder(this)
                      .SetContentTitle("חתונות")
                      .SetContentText(alert)                    
                      .SetContentIntent(contentIntent)
                      .SetSmallIcon(Resource.Drawable.WedType);

                // Build the notification:
                Notification notification = builder.Build();

                // Get the notification manager:
                var notificationManager =
                    GetSystemService(Context.NotificationService) as NotificationManager;

                // Publish the notification:
                const int notificationId = 0;
                notificationManager.Notify(notificationId, notification);

            }
            catch (Exception ex)
            {
                Log.Warn(MainTag, "cant show alert ex message:" + ex.Message);
            }
        }

        private void DisplayNotificationWithObjectId(string objectId)
        {
           try{
               // Instantiate the builder and set notification elements:
               Notification.Builder builder = new Notification.Builder(this)
                   .SetContentTitle("מזל טוב")
                   .SetContentText("Hello World! This is my first notification!")
                   .SetSmallIcon(Resource.Drawable.WedType);

               // Build the notification:
               Notification notification = builder.Build();

               // Get the notification manager:
               NotificationManager notificationManager =
                   GetSystemService(Context.NotificationService) as NotificationManager;

               // Publish the notification:
               const int notificationId = 0;
               notificationManager.Notify(notificationId, notification);

            }
            catch (Exception ex)
            {
                Log.Warn(MainTag, "Error init notification ,ex messaeg :" + ex.Message);
            }
        }
    }
}

