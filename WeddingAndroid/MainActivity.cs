﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Shared;
using Parse;
using Android.Content.PM;
using Shared.Helpers;
using WeddingAndroid.Activities;

namespace WeddingAndroid
{
	[Activity (Label = "WeddingAndroid", MainLauncher = true, Icon = "@drawable/icon",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen",ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : Activity
	{
		ProgressDialog  progressDialog;

		private ServerHelperUtil _serverHelperUtil;
		private ListView _eventsListView;

        private GestureDetector _gestureDetector;

        private UserHelper userHelper;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			_serverHelperUtil = ServerHelperUtil.GetInstance ();
		    userHelper = new UserHelper();


			_eventsListView = FindViewById<ListView> (Resource.Id.EventListView);

            
			_eventsListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) => {
				var activity  = new Intent (this, typeof(EventActivity));
				string id = _serverHelperUtil._comingWeddings[e.Position].ObjectId;

				activity.PutExtra ("EventId", id);
				StartActivity (activity);
			};

			//fill example data

			//CreateWedding (DateTime.Today.Date,"גוני", "גוואניה");


		GetComingWeddings ();

			var calButton = (ImageButton)FindViewById (Resource.Id.calButton);
			if (calButton != null) {
				calButton.Click += (object sender, EventArgs e) => {
					StartActivity (typeof(WeddingCalendar));
				};
			}

			var aboutButton = (ImageButton)FindViewById (Resource.Id.iButton);
			if (aboutButton != null) {
				aboutButton.Click += (object sender, EventArgs e) => {
					StartActivity (typeof(AboutActivity));
				};
			}

            var adminButton = (ImageButton)FindViewById(Resource.Id.adminButton);

            adminButton.Visibility = userHelper.IsAdmin() ? ViewStates.Visible : ViewStates.Gone;

            if (adminButton != null)
            {
                adminButton.Click += (object sender, EventArgs e) => StartActivity(typeof(AdminActivity));
            }


           
           

		}

        protected override void OnResume()
        {
            base.OnResume();
            if (_serverHelperUtil._comingWeddings!=null)
                UpdataList(_serverHelperUtil._comingWeddings);
        }

		private async void CreateWedding(DateTime date , string gname , string bname){
			try{
				var bigObject = new ParseObject("WeddingEvents");


		

				bigObject["Date"] = date;
				bigObject["GroomName"] = "איציק האפסו";
				bigObject["BrideName"] = "שרה נאתכו";
				bigObject["EventType"] = Constants.ENG;

				await bigObject.SaveAsync();
				 bigObject = new ParseObject("WeddingEvents");

				bigObject["Date"] = date;
				bigObject["GroomName"] = "נארת חאכו";
				bigObject["BrideName"] = "ליסה נאתכו";
				bigObject["EventType"] = Constants.ENG;

				await bigObject.SaveAsync();
				bigObject = new ParseObject("WeddingEvents");
				bigObject["Date"] = date;
				bigObject["GroomName"] = "איציק האפסו";
				bigObject["BrideName"] = "שרה נאתכו";
				bigObject["EventType"] = Constants.WED;

				await bigObject.SaveAsync();
				bigObject = new ParseObject("WeddingEvents");
				bigObject["Date"] = date;
				bigObject["GroomName"] = "נארת חאכו";
				bigObject["BrideName"] = "ליסה נאתכו";
				bigObject["EventType"] = Constants.WED;

	



				await bigObject.SaveAsync();
			}catch(Exception ex){
				Toast.MakeText (this, ex.Message, ToastLength.Long).Show();
			}
		}

		private async void GetComingWeddings(){
			progressDialog = ProgressDialog.Show(this, "Please wait...", "Getting  Weddings...", true);

			var result = await _serverHelperUtil.GetComingWeddingEvents ();

            

		    UpdataList(result);

			progressDialog.Hide ();

		}

	    private void UpdataList(List<Event> events)
	    {
            var eventsAdapter = new EventsAdapter(this, events.Where(t => t.IsConfirmd).ToList());

            _eventsListView.Adapter = eventsAdapter;
	    }


	

	}
}


