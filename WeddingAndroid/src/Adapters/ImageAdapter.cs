﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;
using Android.Graphics;
using Parse;

using Shared;
using Uri = Android.Net.Uri;

namespace WeddingAndroid
{
	public class ImageAdapter : BaseAdapter
	{
		List<GalleryItem> _imageList;
		Activity _activity;
		public ImageAdapter(Activity activity ,List<GalleryItem> _imageList ){
			_activity = activity;

			this._imageList = _imageList;
			//	FillEvents ();
		}


		private void FillEvents(){
			_imageList = new List<GalleryItem> ();




		}


		public override int Count {
			get { return _imageList.Count; }
		}

		public override Java.Lang.Object GetItem (int position) {
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}

		public override long GetItemId (int position) {
			return _imageList [position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? _activity.LayoutInflater.Inflate (
				Resource.Layout.Images, parent, false);

			var eventImage = view.FindViewById<ImageView> (Resource.Id.imageView1);

			var imageBitmap = BitmapFactory.DecodeByteArray(_imageList [position].byteFile, 0, _imageList [position].byteFile.Length);

			eventImage.SetImageBitmap(imageBitmap);
			return view;
		}




	}

}

