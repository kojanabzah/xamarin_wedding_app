using System;
using System.Collections.Generic;
using System.Globalization;
using Android.App;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Shared;
using Shared.Helpers;
using WeddingAndroid.Activities;

namespace WeddingAndroid.Adapters
{

    public class EventConfirmationAdapter : BaseAdapter
    {
        List<Event> _eventList;
        AdminActivity _activity;
        private AdminHelper _adminHelper;

        public EventConfirmationAdapter(AdminActivity activity, List<Event> events)
        {
			_activity = activity;
			_eventList = events;

            _adminHelper = AdminHelper.GetInstance();

		}




		public override int Count {
			get { return _eventList.Count; }
		}

		public override Java.Lang.Object GetItem (int position) {
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}

		public override long GetItemId (int position) {
			return _eventList [position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? _activity.LayoutInflater.Inflate (
				Resource.Layout.ConfirmItem, parent, false);
			var eventText = view.FindViewById<TextView> (Resource.Id.EventText);
			var eventImage = view.FindViewById<ImageView> (Resource.Id.EventTypeImage);
			var dateNumberText = view.FindViewById<TextView> (Resource.Id.dayNumberDateText);
			var dateStringText = view.FindViewById<TextView> (Resource.Id.dayStringDateText);

			var circle = view.FindViewById<FrameLayout> (Resource.Id.circleFrameLayout);

            var yes = view.FindViewById<ImageView>(Resource.Id.ConfirmYesImage);
            var no = view.FindViewById<ImageView>(Resource.Id.ConfirmNoImage);

		    yes.Click += (sender, args) =>
		    {
		        confirmEvent(_eventList[position].ObjectId);
		    };
			var date = _eventList [position].Date;

			if (_eventList [position].DisplayText != "") {

				eventText.Text = _eventList [position].DisplayText;

				if (_eventList [position].type == Constants.ENG)
					eventImage.SetImageResource (Resource.Drawable.EngType);
				else
					eventImage.SetImageResource (Resource.Drawable.WedType);



				SetCustomFont (eventText,"Fonts/tahoma.ttf");

				dateNumberText.Text = date.Day.ToString ();

				string dayText = date.ToString ("dddd", new CultureInfo ("he-IL"));
				dateStringText.Text = dayText.Remove (0, 4);

			} else {
				var monthLayout = view.FindViewById<RelativeLayout> (Resource.Id.monthlinearLayout);



				eventText.Visibility = ViewStates.Gone;
				eventImage.Visibility = ViewStates.Gone;
				dateNumberText.Visibility = ViewStates.Gone;
				dateStringText.Visibility = ViewStates.Gone;
				circle.Visibility = ViewStates.Gone;


				var monthText = view.FindViewById<TextView> (Resource.Id.monthDateListText);
				SetCustomFont (monthText,"Fonts/tahomabd.ttf");
				monthText.Text = date.ToString ("MMMM", new CultureInfo ("he-IL"));
				monthText.Visibility = ViewStates.Visible;
				monthLayout.Visibility =  ViewStates.Visible;
			}

			return view;
		}

        private void confirmEvent(string onjectId)
        {
            _adminHelper.ConfirmEvent(onjectId);

            //Notifay all users that new event hapening 
            var notify = new NotificationHelper();
            notify.SendNotificationToAll("����� ���� �����");

        }

		public void SetCustomFont(TextView text,string asset)
		{
			Typeface tf;
			try
			{

				tf = Typeface.CreateFromAsset(Application.Context.Assets,asset);
			}
			catch (Exception e)
			{
				return;
			}

			if (null == tf) return;

			var tfStyle = TypefaceStyle.Normal;

			text.SetTypeface(tf, tfStyle);
		}
    }
}