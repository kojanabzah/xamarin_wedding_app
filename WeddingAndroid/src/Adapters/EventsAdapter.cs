﻿using System;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Shared;
using System.Globalization;
using Android.Graphics;
using Android.Util;
using Android.Nfc;



namespace WeddingAndroid
{
	public class EventsAdapter : BaseAdapter
	{


		List<Event> _eventList;
		Activity _activity;
		public EventsAdapter(Activity activity,List<Event> events ){
			_activity = activity;
			_eventList = events;
			//FillEvents ();
		}


		private void FillEvents(){
			//_eventList = new List<Event> ();
			/*_eventList.Add (new Event (){ Id = 1, DisplayText = "יוסי דמיראג - חווה אלברט ", Date = "fds" });
			_eventList.Add (new Event (){ Id = 2, DisplayText = "אבי שמש -שמש לופז", Date = "fds" });
			_eventList.Add (new Event (){ Id = 3, DisplayText = "יוסי- יוסף", Date = "fds" });
			_eventList.Add (new Event (){ Id = 4, DisplayText = "יוסי דמיראג - חווה אלברט ", Date = "fds" });
			_eventList.Add (new Event (){ Id = 5, DisplayText = "אבי שמש -שמש לופז", Date = "fds" });
			_eventList.Add (new Event (){ Id = 6, DisplayText = "יוסי- יוסף", Date = "fds" });*/

		}


		public override int Count {
			get { return _eventList.Count; }
		}

		public override Java.Lang.Object GetItem (int position) {
			// could wrap a Contact in a Java.Lang.Object
			// to return it here if needed
			return null;
		}

		public override long GetItemId (int position) {
			return _eventList [position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? _activity.LayoutInflater.Inflate (
				Resource.Layout.MyList, parent, false);
			var eventText = view.FindViewById<TextView> (Resource.Id.EventText);
			var eventImage = view.FindViewById<ImageView> (Resource.Id.EventTypeImage);
			var dateNumberText = view.FindViewById<TextView> (Resource.Id.dayNumberDateText);
			var dateStringText = view.FindViewById<TextView> (Resource.Id.dayStringDateText);

			var circle = view.FindViewById<FrameLayout> (Resource.Id.circleFrameLayout);
            var monthLayout = view.FindViewById<RelativeLayout>(Resource.Id.monthlinearLayout);

			var date = _eventList [position].Date;

			if (_eventList [position].DisplayText != "") {
                monthLayout.Visibility = ViewStates.Gone;
                eventText.Visibility = ViewStates.Visible;
                eventImage.Visibility = ViewStates.Visible;
                dateNumberText.Visibility = ViewStates.Visible;
                dateStringText.Visibility = ViewStates.Visible;
                circle.Visibility = ViewStates.Visible;
				eventText.Text = _eventList [position].DisplayText;

				if (_eventList [position].type == Constants.ENG)
					eventImage.SetImageResource (Resource.Drawable.EngType);
				else
					eventImage.SetImageResource (Resource.Drawable.WedType);



				SetCustomFont (eventText,"Fonts/tahoma.ttf");

				dateNumberText.Text = date.Day.ToString ();

				string dayText = date.ToString ("dddd", new CultureInfo ("he-IL"));
				dateStringText.Text = dayText.Remove (0, 4);

			} else {
				



				eventText.Visibility = ViewStates.Gone;
				eventImage.Visibility = ViewStates.Gone;
				dateNumberText.Visibility = ViewStates.Gone;
				dateStringText.Visibility = ViewStates.Gone;
				circle.Visibility = ViewStates.Gone;


				var monthText = view.FindViewById<TextView> (Resource.Id.monthDateListText);
				SetCustomFont (monthText,"Fonts/tahomabd.ttf");
				monthText.Text = date.ToString ("MMMM", new CultureInfo ("he-IL"));
				monthText.Visibility = ViewStates.Visible;
				monthLayout.Visibility =  ViewStates.Visible;
			}

			return view;
		}

		public void SetCustomFont(TextView text,string asset)
		{
			Typeface tf;
			try
			{

				tf = Typeface.CreateFromAsset(Application.Context.Assets,asset);
			}
			catch (Exception e)
			{
				return;
			}

			if (null == tf) return;

			var tfStyle = TypefaceStyle.Normal;

			text.SetTypeface(tf, tfStyle);
		}


	}
}

