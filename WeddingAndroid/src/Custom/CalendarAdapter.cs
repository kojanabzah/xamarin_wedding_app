﻿using System;
using Android.Content;
using Java.Util;
using Android.Widget;
using Android.Views;
using System.Collections.Generic;

namespace WeddingAndroid
{
	public class CalendarAdapter : BaseAdapter
	{
		static readonly int FIRST_DAY_OF_WEEK =0; 

		private Context mContext;
		private Calendar month;
		public  Calendar selectedDate;
		private List<String> items;


		public  List<string> weddingEvents;

		// references to our items
		public String[] days;



	

		public CalendarAdapter (Context c, Calendar monthCalendar)
		{
			mContext = c;
			month = monthCalendar;
			selectedDate = (Calendar)monthCalendar.Clone();

			items = new List<string> ();
			weddingEvents = new List<string> ();

			RefreshDays ();

		}

		public void SetItems(List<String> items) {
			for (int i = 0; i != items.Count; i++) {
				if (items[i].Length == 1) {
					items[i] = "0" + items[i];
				}
			}
			this.items = items;
		}


		public void RefreshDays(){
			// clear items
			items.Clear();

			int lastDay = month.GetActualMaximum(CalendarField.DayOfMonth);
			int firstDay = (int)month.Get(CalendarField.DayOfWeek);

			// figure size of the array
			if(firstDay==1){
				days = new String[lastDay+(FIRST_DAY_OF_WEEK*6)];
			}
			else {
				days = new String[lastDay+firstDay-(FIRST_DAY_OF_WEEK+1)];
			}

			int j=FIRST_DAY_OF_WEEK;

			// populate empty days before first real day
			if(firstDay>1) {
				for(j=0;j<firstDay-FIRST_DAY_OF_WEEK;j++) {
					days[j] = "";
				}
			}
			else {
				for(j=0;j<FIRST_DAY_OF_WEEK*6;j++) {
					days[j] = "";
				}
				j=FIRST_DAY_OF_WEEK*6+1; // sunday => 1, monday => 7
			}

			// populate days
			int dayNumber = 1;
			for(int i=j-1;i<days.Length;i++) {
				days[i] = ""+dayNumber;
				dayNumber++;
			}

	

		}

		public override int Count {
			get { return days.Length; }
		}


		public override Java.Lang.Object GetItem(int position) {

			return null;
		}

		public override long GetItemId(int position) {
			return 0;
		}

		public override View GetView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			TextView dayView;
			if (convertView == null) {  // if it's not recycled, initialize some attributes
				LayoutInflater vi = (LayoutInflater)mContext.GetSystemService(Context.LayoutInflaterService);
				v = vi.Inflate(Resource.Layout.calendar_item, null);

			}
			dayView = (TextView)v.FindViewById(Resource.Id.date);

			// disable empty days from the beginning
			if(days[position].Equals("")) {
				dayView.Clickable = false;
				dayView.Focusable = false;
			}
			else {
				// mark current day as focused
				if(month.Get(CalendarField.Year)== selectedDate.Get(CalendarField.Year) && month.Get(CalendarField.Month)== selectedDate.Get(CalendarField.Month) && days[position].Equals(""+selectedDate.Get(CalendarField.DayOfMonth))) {
					//v.SetBackgroundResource(Resource.Drawable.item_background_focused);
				}
				else {
					//v.SetBackgroundResource(Resource.Drawable.list_item_background);
				}
			}
			dayView.Text = days[position];

			// create date string for comparison
			String date = days[position];

			if(date.Length==1) {
				date = "0"+date;
			}
			String monthStr = ""+(month.Get(CalendarField.Month)+1);
			if(monthStr.Length==1) {
				monthStr = "0"+monthStr;
			}

			// show icon if date is not empty and it exists in the items array
			TextView iw = (TextView)v.FindViewById(Resource.Id.namesTV);

			if(date.Length>0 && weddingEvents!=null ) {     
				var checkDate = (month.Get (CalendarField.Month)+1).ToString();
				if (checkDate.Length == 1) {
					checkDate = "0" + checkDate;
				}

				var item = weddingEvents.Find (x => x.StartsWith (checkDate+date));
                
				if (item != null)
				{

				    var disName = item.Remove(0, 4);
				    var id = disName.Split('/')[1];
                    TextView IDtv = (TextView)v.FindViewById(Resource.Id.idTV);
				    IDtv.Text = id;

                    iw.Text = disName.Split('/')[0];
					iw.Visibility = ViewStates.Visible;
				} else {
					iw.Visibility = ViewStates.Invisible;
				}
			}
			else {
				iw.Visibility = ViewStates.Invisible;
			}
			return v;
		}
	}
}

