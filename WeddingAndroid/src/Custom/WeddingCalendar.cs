﻿using System;
using Android.Content.PM;
using Android.Widget;
using Android.Content;
using Android.Views;
using Android.App;
using Java.Util;
using Android.OS;
using System.Collections.Generic;
using Shared;

namespace WeddingAndroid
{
	[Activity (Label = "CalendarActivity",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen",ScreenOrientation = ScreenOrientation.Portrait)]		
	public class WeddingCalendar : Activity
	{
		
		public Calendar month;
		public CalendarAdapter adapter;
		public Handler handler;
		public List<string> items; // container to store some random calendar items


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.Calendar);

            month = Calendar.GetInstance(Java.Util.TimeZone.Default); //Jan = 0, Feb = 1

		    

			OnNewIntent(Intent);

			var weddings = ServerHelperUtil.GetInstance ().GetWeddings ();
			items = new List<string>();
			foreach (var wed in weddings) {
                if (wed.GroomName != "" && wed.BrideName != "" && wed.IsConfirmd)
			    {
			        var date = wed.Date.Day.ToString();
			        var monthDate = wed.Date.Month.ToString();
			        var id = wed.ObjectId;


			        if (monthDate.Length == 1)
			        {
			            monthDate = "0" + monthDate;
			        }

			        if (date.Length == 1)
			        {
			            date = "0" + date;
			        }
			        var names = string.Format("{0}&{1}", wed.GroomName.Split(' ')[0], wed.BrideName.Split(' ')[0]);
			        items.Add(monthDate + date + names +"/"+id);
			    }
			}



		

			adapter = new CalendarAdapter(this, month);
			adapter.weddingEvents = items;
			GridView gridview = (GridView) FindViewById(Resource.Id.gridview);
			try{
			gridview.Adapter = adapter;

			}catch(Exception ex){
				Console.Write (ex.Message);
			}

			TextView previous  = (TextView) FindViewById(Resource.Id.previous);

			previous.Click += (object sender, EventArgs e) => {
				if(month.Get(CalendarField.Month)== month.GetActualMinimum(CalendarField.Month)) {				
					month.Set((month.Get(CalendarField.Year)-1),month.GetActualMaximum(CalendarField.Month),1);
				} else {
					month.Set(CalendarField.Month,month.Get(CalendarField.Month)-1);
				}
				refreshCalendar();
			};


			TextView next  = (TextView) FindViewById(Resource.Id.next);

			next.Click += (object sender, EventArgs e) => {
				if(month.Get(CalendarField.Month)== month.GetActualMinimum(CalendarField.Month)) {				
					month.Set((month.Get(CalendarField.Year)+1),month.GetActualMaximum(CalendarField.Month),1);
				} else {
					month.Set(CalendarField.Month,month.Get(CalendarField.Month)+1);
				}
				refreshCalendar();
			};

			gridview.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
			{

                View view = e.View;

                var idTV = (TextView) view.FindViewById(Resource.Id.idTV);


			    var isInUse = !string.IsNullOrEmpty(idTV.Text);

			    if (isInUse)
			    {

			        var activity = new Intent(this, typeof (EventActivity));
			        string id = idTV.Text;

			        activity.PutExtra("EventId", id);
			        StartActivity(activity);

			    }


			    else
			    {



			        var dayView = (TextView) view.FindViewById(Resource.Id.date);
			        var day = dayView.Text;

			        var selectedMonth = month.Get(CalendarField.Month).ToString();

			        var activity = new Intent(this, typeof (AddNewEvent));
			        var id = month.Get(CalendarField.Year).ToString() + "/" + selectedMonth + "/" + day;
			        activity.PutExtra("selectedDate", id);
			        StartActivity(activity);
			    }
			};

			refreshCalendar ();

		}


		public void OnNewIntent(Intent intent) {
		//	String date = intent.getStringExtra("date");
			//String[] dateArr = date.split("-"); // date format is yyyy-mm-dd
		//	month.set(Integer.parseInt(dateArr[0]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[2]));
		}


		public void refreshCalendar()
		{
			TextView title  = (TextView) FindViewById(Resource.Id.title);

			adapter.RefreshDays();
			adapter.NotifyDataSetChanged();				
			//handler.post(calendarUpdater); // generate some random calendar items				


            //Jan = 0, Feb = 1
			title.Text =  (month.Get (CalendarField.Month) +1).ToString () +"."+ month.Get (CalendarField.Year).ToString();
		}





	}
}

