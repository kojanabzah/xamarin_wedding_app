using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Shared.Helpers;
using WeddingAndroid.Adapters;

namespace WeddingAndroid.Activities
{
    [Activity(Label = "AdminActivity", Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen", ScreenOrientation = ScreenOrientation.Portrait)]
    public class AdminActivity : Activity
    {
        private ListView _confirmList;
        private AdminHelper _adminHelper;

        private ListView _eventsListView;



        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.AdminView);

            _adminHelper = AdminHelper.GetInstance();

            _adminHelper.ConfirmationListChanged +=  ConfirmationListChanged;

            initializeAdminActivity();
        }

        private void ConfirmationListChanged(object sender, EventArgs eventArgs)
        {
            RefreshList();
        }

        private void initializeAdminActivity()
        {

            _eventsListView = (ListView) FindViewById(Resource.Id.conrimList);
            var list = _adminHelper.GetWaitingConfirmationList();

            var eventConfirmationAdapter = new EventConfirmationAdapter(this, list);

            _eventsListView.Adapter = eventConfirmationAdapter;


        }

        private void RefreshList()
        {
            var list = _adminHelper.GetWaitingConfirmationList();

            var eventConfirmationAdapter = new EventConfirmationAdapter(this, list);

            _eventsListView.Adapter = eventConfirmationAdapter;
        }
    }
}