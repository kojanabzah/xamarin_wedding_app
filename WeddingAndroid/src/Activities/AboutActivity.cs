﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Parse;
using Xamarin.Auth;

namespace WeddingAndroid
{
	[Activity (Label = "AboutActivity",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen")]			
	public class AboutActivity : Activity
	{
        ProgressDialog progressDialog;

	    private bool _loginOnlyonce;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.About);

			initializeAbout();
		}

		private void initializeAbout()
		{

		    var adminLogin = (Button) FindViewById(Resource.Id.adminLogin);
		    adminLogin.Click += (sender, args) =>
		    {
		        if (ParseUser.CurrentUser == null)
		        {
		            LoginToFacebook();
		        }

		    };

		}

        void LoginToFacebook()
        {

            var auth = new OAuth2Authenticator(
                clientId: "479500575549789",
                scope: "",
                authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
                redirectUrl: new Uri("http://www.facebook.com/connect/login_success.html"));

            // If authorization succeeds or is canceled, .Completed will be fired.
            auth.Completed += LoginComplete;

            StartActivity(auth.GetUI(this));
        }


	    public async void LoginComplete(object sender, AuthenticatorCompletedEventArgs e)
	    {
	        if (_loginOnlyonce)
	            return;
	        _loginOnlyonce = true;

	        if (!e.IsAuthenticated)
	        {
	            Console.WriteLine("Not Authorised");
	            return;
	        }
            progressDialog = ProgressDialog.Show(this, "Please wait...", "Getting  Weddings...", true);

	        var accessToken = e.Account.Properties["access_token"].ToString();
	        var expiresIn = Convert.ToDouble(e.Account.Properties["expires_in"]);
	        var expiryDate = DateTime.Now + TimeSpan.FromSeconds(expiresIn);

	        // Now that we're logged in, make a OAuth2 request to get the user's id.
	        var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me"), null, e.Account);
	        var response = await request.GetResponseAsync();

	        var obj = JsonValue.Parse(response.GetResponseText());

	        if (obj == null)
	        {
	            Console.WriteLine("Not Authorised,obj is null");
	            return;

	        }
	        var id = obj["id"].ToString().Replace("\"", ""); // Id has extraneous quotation marks

	        try
	        {

	            var user = await ParseFacebookUtils.LogInAsync(id, accessToken, expiryDate);

	            if (!user.Keys.Contains("UserType"))
	            {
	                user.Add("UserType", "Administrator");
	                await user.SaveAsync();
	            }

                var installation = ParseInstallation.CurrentInstallation;
                installation.Channels = new List<string> { "Admins" };
                await installation.SaveAsync();


                //   await ParsePush.SubscribeAsync("Admins");

                progressDialog.Hide();

	            StartActivity(typeof (MainActivity)); //back home



	        }

	        catch (Exception ex)
	        {

	            Console.WriteLine(ex.Message);
	            return;
	        }
	        finally
	        {
	            progressDialog.Hide();
	        }
            
	    }
	}
}

