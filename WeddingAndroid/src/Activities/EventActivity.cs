﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Environment = Android.OS.Environment;
using Android.Provider;
using Uri = Android.Net.Uri;
using System.Threading;
using Parse;

using Java.IO;

using Shared;

using Android.Graphics;


namespace WeddingAndroid
{
	[Activity (Label = "EventActivity",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen")]			
	public class EventActivity : Activity
	{
		private ServerHelperUtil _serverHelperUtil;





		private TextView brideNameTextView;
		private TextView gromeNameTextView;

		private TextView dateTextView;

		private string eventId;

		private Event thisEvent;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.Event);
			dateTextView = (TextView)FindViewById (Resource.Id.dateTextView);
			eventId = Intent.GetStringExtra ("EventId");
			_serverHelperUtil = ServerHelperUtil.GetInstance ();
			thisEvent = _serverHelperUtil._comingWeddings.Find (x => x.ObjectId == eventId);
			_serverHelperUtil = ServerHelperUtil.GetInstance ();
			GetGallaryImages ();

			brideNameTextView = (TextView)FindViewById (Resource.Id.lefttextView);
			SetCustomFont (brideNameTextView,"Fonts/keteryg-bold-webfont.ttf");
			brideNameTextView.Text = thisEvent.BrideName;
			gromeNameTextView = (TextView)FindViewById (Resource.Id.righttextView);
			gromeNameTextView.Text = thisEvent.GroomName;
			SetCustomFont (gromeNameTextView,"Fonts/keteryg-bold-webfont.ttf");


			dateTextView.Text = thisEvent.Date.ToString("MM/dd/yyyy");

			var camButton = (ImageButton)FindViewById (Resource.Id.imageButton2);

			camButton.Click += TakeAPicture;

			CreateDirectoryForPictures ();

			var gallButton = (ImageButton)FindViewById (Resource.Id.imageButton4);



			gallButton.Click += (object sender, EventArgs e) => {
				
				StartActivity (typeof(EventGalleryActivity));
			};


		}

		private async void GetGallaryImages(){
			DisableGallary ();
			ImageButton gallaryBtn = (ImageButton)FindViewById (Resource.Id.imageButton4);
			_serverHelperUtil.GetEventImages ("Wedding test"); //TODO send the currect id 

			//EnableGallary ();

		}

		private void EnableGallary(){
			ImageButton gallaryBtn = (ImageButton)FindViewById (Resource.Id.imageButton4);

			gallaryBtn.Enabled = true;
		}
		private void DisableGallary(){
			ImageButton gallaryBtn = (ImageButton)FindViewById (Resource.Id.imageButton4);

			gallaryBtn.Enabled = false;;

		}



		private void TakeAPicture (object sender, EventArgs eventArgs)
		{
			Intent intent = new Intent (MediaStore.ActionImageCapture);
			App._file = new File (App._dir, String.Format("myPhoto_{0}.jpg", Guid.NewGuid()));
			intent.PutExtra (MediaStore.ExtraOutput, Uri.FromFile (App._file));
			StartActivityForResult (intent, 0);

		}

		private void CreateDirectoryForPictures ()
		{
			try{
				App._dir = new File (
					Environment.GetExternalStoragePublicDirectory (
						Environment.DirectoryPictures), "CameraAppDemo");
				if (!App._dir.Exists ())
				{
					App._dir.Mkdirs( );
				}
			}catch(Exception ex){
				Toast.MakeText (this, ex.Message, ToastLength.Long).Show ();
			}
		}




		ProgressDialog  progressDialog;
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			// Make it available in the gallery
			if (resultCode != Result.Canceled){


				Intent mediaScanIntent = new Intent (Intent.ActionMediaScannerScanFile);
				Uri contentUri = Uri.FromFile (App._file);
				mediaScanIntent.SetData (contentUri);
				SendBroadcast (mediaScanIntent);



				progressDialog = ProgressDialog.Show(this, "Please wait...", "Uploading Image...", true);
				new Thread(new ThreadStart(delegate
					{
						//LOAD METHOD TO GET ACCOUNT INFO
						RunOnUiThread(() =>  UploadImage() );

					})).Start();



				//	}

				// Dispose of the Java side bitmap.


			}
		}

		public void SetCustomFont(TextView text,string asset)
		{
			Typeface tf;
			try
			{

				tf = Typeface.CreateFromAsset(Application.Context.Assets,asset);
			}
			catch (Exception e)
			{
				return;
			}

			if (null == tf) return;

			var tfStyle = TypefaceStyle.Normal;

			text.SetTypeface(tf, tfStyle);
		}



		public async void   UploadImage(){

			try{
				var testObject = new ParseObject ("WeddingImages");
				int height = 150;
				int width = 150;
				App.bitmap = App._file.Path.LoadAndResizeBitmap (width, height);

				byte[] data = BitmapHelpers.ConvertBitMapToByteArray(App.bitmap);

				ParseFile file = new ParseFile("myImage", data);
				await file.SaveAsync();


				testObject ["image"] = file;
				await testObject.SaveAsync ();


				App.bitmap = null;
				GC.Collect();
			}catch(Exception e){
				progressDialog.Hide();
			}

			//_serverHelperUtil.IsRefreshNeeded = true;
			GetGallaryImages();

			progressDialog.Hide();
		}


	}
}

