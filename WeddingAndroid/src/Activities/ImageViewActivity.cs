﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Shared;

namespace WeddingAndroid
{
	[Activity (Label = "ImageViewActivity",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen")]			
	public class ImageViewActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.ShowImage);
			// Create your application here

			var selectedImage = (ImageView)FindViewById (Resource.Id.showImageView);
			try{
				long imageId = Intent.GetLongExtra ("imageId",-1);

				if (imageId != -1) {
					var image = ServerHelperUtil.GetInstance().GetImage(imageId);

					var imageBitmap = BitmapFactory.DecodeByteArray(image.byteFile, 0, image.byteFile.Length);

					selectedImage.SetImageBitmap(imageBitmap);
				}

			}catch(Exception ex){
				if(ex.Message !=null){
				}
				//	progressDialog.Hide();
			}
		}
	}
}

