﻿using System;
using System.Collections.Generic;
using System.Json;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Widget;
using Parse;
using Shared;
using Shared.Helpers;
using Xamarin.Auth;

namespace WeddingAndroid
{
    [Activity(Label = "AddNewEvent", Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen", ScreenOrientation = ScreenOrientation.Portrait)]			
	public class AddNewEvent : Activity
	{

		private DateTime date;
		private string[] _selectetDate;
        private bool _loginOnlyonce;

        private string _groomName;
        private string _groomParentsName;
        private string _brideName;
        private string _brideParentsName;
        private string _weedingLocation;


        ProgressDialog progressDialog;

        private ImageView _weddingType;

        private string eventType;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.AddNewEvent);



			InitializeAddNewEvent();
		}

		private void InitializeAddNewEvent(){
			_selectetDate = Intent.GetStringExtra ("selectedDate").Split('/');
			var year = _selectetDate[0];
			var month=_selectetDate[1];
			var day=_selectetDate[2];
	
			var dateTextView = (TextView)FindViewById (Resource.Id.dateTextView);
			dateTextView.Text = string.Format ("{0}/{1}/{2}", day, month, year);

            date = new DateTime(int.Parse(year), int.Parse(month) + 1, int.Parse(day)); //TODO try parse

            var confirm = (FrameLayout)FindViewById(Resource.Id.frameLayout1);
            confirm.Click += (sender, e) => CreateNewEvent();

            _weddingType = (ImageView)FindViewById(Resource.Id.weddingTypeImage);

		    eventType = Constants.ENG;

            _weddingType.Click += delegate(object sender, EventArgs args)
            {
                if (eventType == Constants.ENG)
                {
                    _weddingType.SetImageResource(Resource.Drawable.WedType);
                    eventType = Constants.WED;
                }
                else
                {
                    _weddingType.SetImageResource(Resource.Drawable.EngType);
                    eventType = Constants.ENG;
                }
            };

		}

        private  async void CreateNewEvent()
        {
             _groomName = ((TextView)FindViewById(Resource.Id.GroomEditText)).Text;
             _brideName = ((TextView)FindViewById(Resource.Id.brideEditText)).Text;
             _groomParentsName = ((TextView)FindViewById(Resource.Id.groomParentsEditText)).Text;
             _brideParentsName = ((TextView)FindViewById(Resource.Id.brideParentsEditText)).Text;
             _weedingLocation = ((TextView)FindViewById(Resource.Id.weddingLocationEditText)).Text;

            //TODO COFIRM WE HAVE ALL INFORMATION 

            if (ParseUser.CurrentUser == null)
                LoginToFacebookAndSaveEnvent();

            else
            {
                SaveEvent();
            }

            

        }

        private async void SaveEvent()
        {
            progressDialog = ProgressDialog.Show(this, "Please wait...", "Getting  Weddings...", true);

            var eventObject = new ParseObject("WeddingEvents");

            eventObject["Date"] = date;
            eventObject["GroomName"] = _groomName;
            eventObject["GroomNameParentNames"] = _groomParentsName;
            eventObject["BrideName"] = _brideName;
            eventObject["BrideNameParentNames"] = _brideParentsName;
            eventObject["EventType"] = eventType;
            eventObject["WeddingLocation"] = _weedingLocation;

            eventObject["IsConfirmed"] = false; //TODO , if am admin maye true , need to think about it 

            await eventObject.SaveAsync();

            progressDialog.Hide();

            //Send Admin notification

            var notify = new NotificationHelper();
            notify.SendNotificationToAdmins("testing notification");

            DoneGoBackHome();
        }

        private void DoneGoBackHome()
        {
            StartActivity(typeof(MainActivity));

        }

        void LoginToFacebookAndSaveEnvent()
        {

            var auth = new OAuth2Authenticator(
                clientId: "479500575549789",
                scope: "",
                authorizeUrl: new Uri("https://m.facebook.com/dialog/oauth/"),
                redirectUrl: new Uri("http://www.facebook.com/connect/login_success.html"));

            // If authorization succeeds or is canceled, .Completed will be fired.
            auth.Completed += LoginComplete;

            StartActivity(auth.GetUI(this));
        }


        public async void LoginComplete(object sender, AuthenticatorCompletedEventArgs e)
        {
            if (_loginOnlyonce)
                return;
            _loginOnlyonce = true;

            if (!e.IsAuthenticated)
            {
                Console.WriteLine("Not Authorised");
                return;
            }

            var accessToken = e.Account.Properties["access_token"].ToString();
            var expiresIn = Convert.ToDouble(e.Account.Properties["expires_in"]);
            var expiryDate = DateTime.Now + TimeSpan.FromSeconds(expiresIn);

            // Now that we're logged in, make a OAuth2 request to get the user's id.
            var request = new OAuth2Request("GET", new Uri("https://graph.facebook.com/me"), null, e.Account);
            var response = await request.GetResponseAsync();

            var obj = JsonValue.Parse(response.GetResponseText());

            if (obj == null)
            {
                Console.WriteLine("Not Authorised,obj is null");
                return;

            }
            var id = obj["id"].ToString().Replace("\"", ""); // Id has extraneous quotation marks

            try
            {
                progressDialog = ProgressDialog.Show(this, "Please wait...", "Saving the date... ", true);

                var user = await ParseFacebookUtils.LogInAsync(id, accessToken, expiryDate);
                if (!user.ContainsKey("GroomName"))
                    user.Add("GroomName", _groomName);
                if (!user.ContainsKey("BrideName"))
                    user.Add("BrideName", _groomName);
                await user.SaveAsync();
                
                SaveEvent();




            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
                return;
            }

            


        }


	}
}

