﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Net.Http;
using Java.IO;
using System.Threading.Tasks;
using Parse;

using Shared;
using System.Threading;

namespace WeddingAndroid
{
	[Activity (Label = "EventGalleryActivity",Theme = "@android:style/Theme.Holo.NoActionBar.Fullscreen")]			
	public class EventGalleryActivity : Activity
	{
		GridView gallaryList;

		ProgressDialog  progressDialog;
		private ServerHelperUtil _serverHelperUtil;

		protected override void OnResume ()
		{
			base.OnResume ();
			if (_serverHelperUtil.IsRefreshNeeded) {
				GetImagesAsync ();
				_serverHelperUtil.IsRefreshNeeded = false;
			}
		}



		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			_serverHelperUtil = ServerHelperUtil.GetInstance ();

			SetContentView(Resource.Layout.EventGallery);




			GetImagesAsync ();





			gallaryList = (GridView)FindViewById (Resource.Id.gallaryGV);


			gallaryList.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) => {
				try{
					var imageActivity = new Intent (this, typeof(ImageViewActivity));
					imageActivity.PutExtra("imageId", _serverHelperUtil.GetEventImageList()[e.Position].Id);
					StartActivity (imageActivity);
				}catch(Exception ex){
					if(ex.Message !=null){
					}
					//	progressDialog.Hide();
				}
			};

		}




		private async void GetImagesAsync(){
			progressDialog = ProgressDialog.Show(this, "Please wait...", "Loading Wedding images...", true);


			var imageList = _serverHelperUtil.GetEventImageList ();

			try{
				foreach (var item in imageList) {
					byte[] byt =  await new HttpClient().GetByteArrayAsync(item.file.Url);
					item.byteFile = byt;
				}


			}
			catch(Exception ex){
				if(ex.Message !=null){
					//TODO
				}
			}

			gallaryList = (GridView)FindViewById (Resource.Id.gallaryGV);
			gallaryList.Adapter = new ImageAdapter (this, imageList);

			progressDialog.Hide(); //Done



		}



	}
}

